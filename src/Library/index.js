import React from 'react';
import { localUrl } from "./Configs";


// export const postMethod = async (data) => {
//     // const id = localStorage.getItem("YbpWWDicin_token");
//     // console.log("token", id);
//     // const token = id ? id : "";
//     // isLoading();
//     // console.log("ApiPost fetch before", data);
//     console.log(data.body)
//     try {
//       const response = await fetch(localUrl + data.url, {
//         method: "POST",
//         headers: data.header,
//         "Content-type": "application/json",
//         body: JSON.stringify(data.body),
//       });
//     //   isNotLoading();
//       return response.json();
//     } catch (err) {
//     //   isNotLoading();
//     //   handleCatchErr(err);
//     }
//   };

  export const postMethod = async (data) => {
   
    try {
      const response = await fetch(localUrl + data.url, {
        method: "POST",
        headers: data.header,
        Accept: "application/json",
        body: data.body,
      });
    //   isNotLoading();
      return response.json();}
      catch (err) {
        //   isNotLoading();
        //   handleCatchErr(err);
        console.log(err);
        }};