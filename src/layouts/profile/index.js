/**
=========================================================
* Material Dashboard 2 React - v2.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/
import React, { useState, useEffect } from "react";
// @mui material components
import Grid from "@mui/material/Grid";
import Divider from "@mui/material/Divider";

// @mui icons
import FacebookIcon from "@mui/icons-material/Facebook";
import TwitterIcon from "@mui/icons-material/Twitter";
import InstagramIcon from "@mui/icons-material/Instagram";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDButton from "components/MDButton";

import Card from "@mui/material/Card";
import TextField from "@mui/material/TextField";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";

// Material Dashboard 2 React example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from "examples/Footer";
import ProfileInfoCard from "examples/Cards/InfoCards/ProfileInfoCard";
import ProfilesList from "examples/Lists/ProfilesList";
import DefaultProjectCard from "examples/Cards/ProjectCards/DefaultProjectCard";
import Icon from "@mui/material/Icon";

// Overview page components
import Header from "layouts/profile/components/Header";
import PlatformSettings from "layouts/profile/components/PlatformSettings";

// Data
import profilesListData from "layouts/profile/data/profilesListData";

import DataTable from "examples/Tables/DataTable";
import lifetimeEngagementData from "layouts/tables/data/lifetimeEngagementData";

//Date-Picker
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import moment from "moment";


// Images
import homeDecor1 from "assets/images/home-decor-1.jpg";
import homeDecor2 from "assets/images/home-decor-2.jpg";
import homeDecor3 from "assets/images/home-decor-3.jpg";
import homeDecor4 from "assets/images/home-decor-4.jpeg";
import team1 from "assets/images/team-1.jpg";
import team2 from "assets/images/team-2.jpg";
import team3 from "assets/images/team-3.jpg";
import team4 from "assets/images/team-4.jpg";

function Overview() {
  const [isLoading, setLoading] = useState(false);

  const [startValue,setStartValue]  = useState(1);
  const [endValue,setEndValue]  = useState(1000);
  const [levelType,setLevelType] = useState("active");

  const valueOneChanged = (event) => {
    setStartValue(event.target.value);
  } 
  const valueTwoChanged = (event) => {
    setEndValue(event.target.value);
  } 

  const valueThreeChanged = (event) => {
    setLevelType(event.target.value);
  }

  const onClickButton = () => {
    // alert(startValue+endValue+levelType);
    lifetimeContentData();
  }

  const [lifetimeContent,setLifetimeContent] = useState();

  const { columns: tColumns, rows: tRows } = lifetimeEngagementData(lifetimeContent);

  const onLoadLifetimeContentData = () => {
    // alert(dateRange[1])
    // const startValue = 1;
    // const endValue = 1000;
    // const levelType = "active";

    let formData = new FormData();
    formData.append("from_range", startValue);
    formData.append("to_range", endValue);
    formData.append("level", levelType);
    var myHeaders = new Headers();
    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formData,
    };
    const fetchData = async () => {
      // setLoading(true);
      const response = await fetch(
        "https://ic-dev.lightersideofrealestate.com/ic-scripts_sync_codes/weekly_reports/api_lifetime_engagement_data.php",
        requestOptions
      );
      const data = await response.json();
      setLifetimeContent({ data });

      setLoading(false);
      // setFdate(formatLastDate);
      // setLdate(formatYesterdayDate);
      // console.log(dateRange[0]);
      // console.log( memeType);
      // console.log(end);
      if (data) {
        console.log(data);
      }
    };

    fetchData();
    // }
  };

  const lifetimeContentData = () => {
    // alert(dateRange[1])
    // const startValue = 1;
    // const endValue = 1000;
    // const levelType = "active";

    let formData = new FormData();
    formData.append("from_range", startValue);
    formData.append("to_range", endValue);
    formData.append("level", levelType);
    var myHeaders = new Headers();
    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formData,
    };
    const fetchData = async () => {
      setLoading(true);
      const response = await fetch(
        "https://ic-dev.lightersideofrealestate.com/ic-scripts_sync_codes/weekly_reports/api_lifetime_engagement_data.php",
        requestOptions
      );
      const data = await response.json();
      setLifetimeContent({ data });

      setLoading(false);
      // setFdate(formatLastDate);
      // setLdate(formatYesterdayDate);
      // console.log(dateRange[0]);
      // console.log( memeType);
      // console.log(end);
      if (data) {
        console.log(data);
      }
    };

    fetchData();
    // }
  };


  useEffect(() => {
    // console.log(startDate);
    onLoadLifetimeContentData();
    
    // return setDateRange([null, null]);
  }, []);



  return (
    <DashboardLayout>
      <MDBox pt={6} pb={3}>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Card>
              <MDBox
                mx={2}
                mt={-3}
                py={3}
                px={2}
                variant="gradient"
                bgColor="primary"
                borderRadius="lg"
                coloredShadow="info"
              >
                <MDTypography variant="h6" color="white">
                Lifetime Engagement Data
                </MDTypography>
              </MDBox>

              <MDBox display="flex">
                <MDBox
                  component="form"
                  sx={{
                    "& .MuiTextField-root": { m: 3, width: "15ch" },
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <TextField
                    id="outlined-number"
                    label="Start Value"
                    type="number"
                    // InputLabelProps={{
                    //   shrink: true,
                    // }}
                    value={startValue}
                    onChange={valueOneChanged}
                  />
                  <TextField
                    id="outlined-number"
                    label="End Value"
                    type="number"
                    // InputLabelProps={{
                    //   shrink: true,
                    // }}
                    value={endValue}
                    onChange={valueTwoChanged}
                  />
                </MDBox>

                <FormControl sx={{ m: 3, minWidth: 130 }}>
                  <InputLabel id="demo-simple-select-helper-label">Level</InputLabel>
                  <Select
                    labelId="demo-simple-select-helper-label"
                    id="demo-simple-select-helper"
                    value={levelType}
                    label="Age"
                    onChange={valueThreeChanged}
                    className="selecting-values"
                    // sx={{padding:"20px"}}
                  >
                    <MenuItem value="active">Active</MenuItem>
                    <MenuItem value="cancel">Cancel</MenuItem>
                    <MenuItem value="overall">Overall</MenuItem>
                  </Select>
                  {/* <FormHelperText>With label + helper text</FormHelperText> */}
                </FormControl>

                <MDBox  sx={{ m: 3}}>
                {!isLoading && (
                  <MDButton
                    variant="gradient"
                    color="info"
                    onClick={onClickButton}
                    className="submit"
                  >
                    &nbsp;Submit
                  </MDButton>
                )}
                {isLoading && (
                  <MDButton
                    variant="gradient"
                    color="info"
                    onClick={onClickButton}
                    disabled
                  >
                    <Icon fontSize="small" color="inherit">
                      loop
                    </Icon>
                    &nbsp;Loading...
                  </MDButton>
                )}

                </MDBox>
               
              </MDBox>

              <MDBox>
                <DataTable
                  table={{ columns: tColumns, rows: tRows }}
                  isSorted={false}
                  entriesPerPage={false}
                  showTotalEntries={false}
                  noEndBorder
                  topHeader={false}
                  headingOne="Engagement levels on each content"
                  headingTwo="Unique users Engagement levels on each content "
                />
              </MDBox>

            </Card>
          </Grid>
        </Grid>
      </MDBox>
    </DashboardLayout>
  );
}

export default Overview;
