import React, { useState, useEffect } from "react";
// @mui material components
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import Icon from "@mui/material/Icon";
import moment from "moment";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDButton from "components/MDButton";
// import CircularProgress from "@mui/material/CircularProgress";
import Header from "components/Header/Header";
import Loader from "components/Loader/Loader";

// Material Dashboard 2 React example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";

import { postMethod } from "Library";

// import DashboardNavbar from "examples/Navbars/DashboardNavbar";
// import Footer from "examples/Footer";
import DataTable from "examples/Tables/DataTable";
import ComplexStatisticsCard from "examples/Cards/StatisticsCards/ComplexStatisticsCard";

// Data
// import authorsTableData from "layouts/tables/data/authorsTableData";
import projectsTableData from "layouts/tables/data/projectsTableData";
import moduleTableData from "layouts/tables/data/moduleTableData";

//Date-Picker
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

function Tables() {
  // const { columns, rows } = authorsTableData();
  const [userData, setUserData] = useState();
  const [uniqueData, setUniqueData] = useState();

  const [holidayContent, setHolidayContent] = useState();
  const [withBranding, setWithBranding] = useState();

  const { columns: pColumns, rows: pRows } = projectsTableData(userData, uniqueData);
  const { columns: mColumns, rows: mRows } = moduleTableData(holidayContent, withBranding);

  const [fdate, setFdate] = useState(null);
  const [ldate, setLdate] = useState(null);

  const [apiData, setApiData] = useState([]);

  // const [userType, setUserType] = useState("meme");

  const [isLoading, setLoading] = useState(false);
  const [valueLoader, setValueLoader] = useState(false);

  const currentDate = new Date();
  //yesterday date - end date
  const yesterdayDate = moment(currentDate).subtract(1, "days");
  const lastDate = yesterdayDate._d;
  const formatYesterdayDate = moment(lastDate).format("YYYY-MM-DD");
  //last seven day date - start date
  const lastSevenDaysDate = moment(currentDate).subtract(1, "week");
  const firstDate = lastSevenDaysDate._d;
  const formatLastDate = moment(firstDate).format("YYYY-MM-DD");

  const [dateRange, setDateRange] = useState([null, null]);
  const [startDate, endDate] = dateRange;

  // const LoadingData = () => {
  //   return (
  //     <MDBox sx={{ display: "flex", justifyContent: "center"}} pt={1}>
  //       <CircularProgress size="1.5rem" color="inherit" />
  //     </MDBox>
  //   );
  // };

  const rangeFormat =
    endDate === null
      ? moment(startDate).format("MM/DD/YYYY")
      : moment(startDate).format("MM/DD/YYYY") +
        " " +
        "-" +
        " " +
        moment(endDate).format("MM/DD/YYYY");

  const rangeDate =
    moment(firstDate).format("MM/DD/YYYY") +
    " " +
    "-" +
    " " +
    moment(lastDate).format("MM/DD/YYYY");

  const complexCardValues = [
    { color: "dark", icon: "leaderboard", title: "Total Article Count" },
    { color: "warning", icon: "leaderboard", title: "Total meme Count" },
    { color: "success", icon: "leaderboard", title: "Total BB Count" },
  ];

  //Onload function
  const onLoadSelectedDate = () => {
    // alert(dateRange[1])
    // if (dateRange[0] !== null) {
    console.log(startDate, endDate);

    const startData = moment(startDate).format("MM/DD/YYYY");
    // const endData = moment(endDate).format("MM/DD/YYYY");

    // const end = endDate === null ? formatLastDate : formatYesterdayDate ;
    // alert(end);
    const type = "meme";
    // if(dateRange[1] !== null) {
    //   endData = startData ;
    // }else {
    //   endData = endData;
    // }

    let formData = new FormData();
    formData.append("from_date", formatLastDate);
    formData.append("to_date", formatYesterdayDate);
    formData.append("engagement_type", type);
    var myHeaders = new Headers();

    const requestOptions = {
      url: "engagement_content.php",
      headers: myHeaders,
      body: formData,
    };

    const fetchData = async (requestOptions) => {
      console.log(requestOptions.body);
      setValueLoader(true);
      // setLoading(true);
      const response = await postMethod(requestOptions);
      // fetch(
      //   "https://ic-dev.lightersideofrealestate.com/ic-scripts_sync_codes/weekly_reports/engagement_content.php",
      //   requestOptions
      // );
      console.log(response);
      if (response.status == 1) {
        // const data = await response.json();
        setUserData(response);
        // aler(data.status+"vale");
        if (response.status === 1) {
          setApiData([
            response.total_article_count,
            response.total_meme_count,
            response.total_bb_count,
          ]);
        }

        setLoading(false);
        setValueLoader(false);
        setFdate(formatLastDate);
        setLdate(formatYesterdayDate);
        console.log(dateRange[0]);
        console.log(response.data.data.total_article_count + "vale");
        // console.log(end);
        if (response.data) {
          console.log(response.data);
        }
      }
    };
    fetchData(requestOptions);
    // }
  };

  //onLoadHolidayData function
  const onLoadHolidayData = () => {
    // alert(dateRange[1])
    // if (dateRange[0] !== null) {
    console.log(startDate, endDate);

    const startData = moment(startDate).format("MM/DD/YYYY");
    // const endData = moment(endDate).format("MM/DD/YYYY");
    const apiTime = "23:59:59";

    // const end = endDate === null ? formatLastDate : formatYesterdayDate ;
    // alert(end);
    // const type = "meme";
    // if(dateRange[1] !== null) {
    //   endData = startData ;
    // }else {
    //   endData = endData;
    // }

    let formData = new FormData();
    formData.append("from_date", formatLastDate);
    formData.append("to_date", formatYesterdayDate + " " + apiTime);
    // formData.append("engagement_type", type);
    var myHeaders = new Headers();
    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formData,
    };
    const fetchData = async () => {
      // setLoading(true);
      console.log(requestOptions.body);
      setValueLoader(true);

      const response = await fetch(
        "https://ic-dev.lightersideofrealestate.com/ic-scripts_sync_codes/weekly_reports/api_meme_engagement_holiday.php",
        requestOptions
      );
      const data = await response.json();
      setHolidayContent({ data });

      setLoading(false);
      setValueLoader(false);
      setFdate(formatLastDate);
      setLdate(formatYesterdayDate);
      console.log(dateRange[0]);
      console.log(startData);
      // console.log(end);
      if (data) {
        console.log(data);
      }
    };
    fetchData();
    // }
  };

  const onLoadSelectedUniqueDate = () => {
    // alert(dateRange[1])
    // if (dateRange[0] !== null) {
    console.log(startDate, endDate);

    const startData = moment(startDate).format("MM/DD/YYYY");
    // const endData = moment(endDate).format("MM/DD/YYYY");

    // const end = endDate === null ? formatLastDate : formatYesterdayDate ;
    // alert(end);
    const type = "user";
    // if(dateRange[1] !== null) {
    //   endData = startData ;
    // }else {
    //   endData = endData;
    // }

    let formData = new FormData();
    formData.append("from_date", formatLastDate);
    formData.append("to_date", formatYesterdayDate);
    formData.append("engagement_type", type);
    var myHeaders = new Headers();
    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formData,
    };
    const fetchData = async () => {
      // setLoading(true);
      setValueLoader(true);
      const response = await fetch(
        "https://ic-dev.lightersideofrealestate.com/ic-scripts_sync_codes/weekly_reports/engagement_content.php",
        requestOptions
      );
      const data = await response.json();
      setUniqueData({ data });
      setLoading(false);
      setValueLoader(false);
      setFdate(formatLastDate);
      setLdate(formatYesterdayDate);
      console.log(dateRange[0]);
      console.log(startData);
      // console.log(end);
      if (data) {
        console.log(data);
      }
    };

    fetchData();
    // }
  };

  const onLoadBrandingDate = () => {
    // alert(dateRange[1])
    // if (dateRange[0] !== null) {
    console.log(startDate, endDate);

    const startData = moment(startDate).format("MM/DD/YYYY");
    // const endData = moment(endDate).format("MM/DD/YYYY");
    const apiTime = "23:59:59";
    // const end = endDate === null ? formatLastDate : formatYesterdayDate ;
    // alert(end);
    // const type = "user";
    // if(dateRange[1] !== null) {
    //   endData = startData ;
    // }else {
    //   endData = endData;
    // }

    let formData = new FormData();
    formData.append("from_date", formatLastDate);
    formData.append("to_date", formatYesterdayDate + " " + apiTime);
    // formData.append("engagement_type", type);
    var myHeaders = new Headers();
    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formData,
    };
    const fetchData = async () => {
      // setLoading(true);
      setValueLoader(true);
      const response = await fetch(
        "https://ic-dev.lightersideofrealestate.com/ic-scripts_sync_codes/weekly_reports/api_brandingbar.php",
        requestOptions
      );
      const data = await response.json();
      setWithBranding({ data });
      setLoading(false);
      setValueLoader(false);
      setFdate(formatLastDate);
      setLdate(formatYesterdayDate);
      console.log(dateRange[0]);
      console.log(startData);
      // console.log(end);
      if (data) {
        console.log(data);
      }
    };

    fetchData();
  };

  useEffect(() => {
    console.log(startDate);
    onLoadSelectedDate();
    onLoadSelectedUniqueDate();
    onLoadHolidayData();
    onLoadBrandingDate();
    return setDateRange([null, null]);
  }, []);

  const configsButton = (
    <MDBox
      display="flex"
      justifyContent="center"
      alignItems="center"
      left="2.15rem"
      height="3.25rem"
      position="absolute"
      zIndex={0}
      color="dark"
      sx={{ cursor: "pointer" }}
      // onClick={handleConfiguratorOpen}
    >
      <Icon fontSize="small" color="inherit">
        event
      </Icon>
    </MDBox>
  );

  //when clicking  SelectedDate Submit button
  const SelectedDate = () => {
    // alert(dateRange[1])
    // if (dateRange[0] !== null) {
    console.log(startDate, endDate);
    const apiTime = "23:59:59";

    const startData = moment(startDate).format("YYYY-MM-DD");
    const endData = moment(endDate).format("YYYY-MM-DD");

    const end = endDate === null ? startData : endData;
    // alert(end);
    const type = "meme";
    // if(dateRange[1] !== null) {
    //   endData = startData ;
    // }else {
    //   endData = endData;
    // }

    let formData = new FormData();
    formData.append("from_date", startData);
    formData.append("to_date", end + " " + apiTime);
    formData.append("engagement_type", type);
    var myHeaders = new Headers();
    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formData,
    };
    const fetchData = async () => {
      //  console.log(formData)
      setLoading(true);
      const response = await fetch(
        "https://ic-dev.lightersideofrealestate.com/ic-scripts_sync_codes/weekly_reports/engagement_content.php",
        requestOptions
      );
      const data = await response.json();
      setUserData({ data });
      setLoading(false);
      console.log(dateRange[0]);
      console.log(startData);
      console.log(end);
      // console.log(userData && userData.data.dau_monthly_count);
      if (data) {
        console.log(data);
      }
    };

    fetchData();
    // }
  };

  //when clicking holidayDate Submit button
  const holidayDate = () => {
    // alert(dateRange[1])
    // if (dateRange[0] !== null) {
    console.log(startDate, endDate);
    const apiTime = "23:59:59";

    const startData = moment(startDate).format("YYYY-MM-DD");
    const endData = moment(endDate).format("YYYY-MM-DD");

    const end = endDate === null ? startData : endData;
    // alert(end);
    // const type = "meme";
    // if(dateRange[1] !== null) {
    //   endData = startData ;
    // }else {
    //   endData = endData;
    // }

    let formData = new FormData();
    formData.append("from_date", startData);
    formData.append("to_date", end + " " + apiTime);
    // formData.append("engagement_type", type);
    var myHeaders = new Headers();
    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formData,
    };
    const fetchData = async () => {
      //  console.log(formData)
      setLoading(true);
      const response = await fetch(
        "https://ic-dev.lightersideofrealestate.com/ic-scripts_sync_codes/weekly_reports/api_meme_engagement_holiday.php",
        requestOptions
      );
      const data = await response.json();
      setHolidayContent({ data });
      setLoading(false);
      console.log(dateRange[0]);
      console.log(startData);
      console.log(end);
      // console.log(userData && userData.data.dau_monthly_count);
      if (data) {
        console.log(data);
      }
    };

    fetchData();
    // }
  };

  //when clicking BrandingDate Submit button
  const BrandingDate = () => {
    // alert(dateRange[1])
    // if (dateRange[0] !== null) {
    console.log(startDate, endDate);
    const apiTime = "23:59:59";

    const startData = moment(startDate).format("YYYY-MM-DD");
    const endData = moment(endDate).format("YYYY-MM-DD");

    const end = endDate === null ? startData : endData;
    // alert(end);
    // const type = "meme";
    // if(dateRange[1] !== null) {
    //   endData = startData ;
    // }else {
    //   endData = endData;
    // }

    let formData = new FormData();
    formData.append("from_date", startData);
    formData.append("to_date", end + " " + apiTime);
    // formData.append("engagement_type", type);
    var myHeaders = new Headers();
    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formData,
    };
    const fetchData = async () => {
      //  console.log(formData)
      setLoading(true);
      const response = await fetch(
        "https://ic-dev.lightersideofrealestate.com/ic-scripts_sync_codes/weekly_reports/api_brandingbar.php",
        requestOptions
      );
      const data = await response.json();
      setWithBranding({ data });
      setLoading(false);
      console.log(dateRange[0]);
      console.log(startData);
      console.log(end);
      // console.log(userData && userData.data.dau_monthly_count);
      if (data) {
        console.log(data);
      }
    };

    fetchData();
    // }
  };

  //when clicking SelectedUniqueDate Submit button
  const SelectedUniqueDate = () => {
    // alert(dateRange[1])
    // if (dateRange[0] !== null) {
    console.log(startDate, endDate);
    const apiTime = "23:59:59";

    const startData = moment(startDate).format("YYYY-MM-DD");
    const endData = moment(endDate).format("YYYY-MM-DD");

    const end = endDate === null ? startData : endData;
    // alert(end);
    const type = "user";
    // if(dateRange[1] !== null) {
    //   endData = startData ;
    // }else {
    //   endData = endData;
    // }

    let formData = new FormData();
    formData.append("from_date", startData);
    formData.append("to_date", end + " " + apiTime);
    formData.append("engagement_type", type);
    var myHeaders = new Headers();
    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formData,
    };
    const fetchData = async () => {
      //  console.log(formData)
      setLoading(true);
      const response = await fetch(
        "https://ic-dev.lightersideofrealestate.com/ic-scripts_sync_codes/weekly_reports/engagement_content.php",
        requestOptions
      );
      const data = await response.json();
      setUniqueData({ data });
      setLoading(false);
      console.log(dateRange[0]);
      console.log(startData);
      console.log(end);
      // console.log(userData && userData.data.dau_monthly_count);
      if (data) {
        console.log(data);
      }
    };

    fetchData();
    // }
  };

  const userClicks = () => {
    SelectedDate();
    SelectedUniqueDate();
    holidayDate();
    BrandingDate();
  };

  return (
    <DashboardLayout>
      <DashboardNavbar />
      <MDBox pt={4} pb={3}>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Card >
              <Header header="Engagement Content" />

              <MDBox py={5} >
                <Grid container spacing={3} pl={2} pr={2}>
                  
                  {console.log(apiData)}
                  {complexCardValues.map((value, index) => {
                    return (
                      <>
                        <Grid item xs={12} md={6} lg={4}>
                          <MDBox mb={1.5} >
                            <ComplexStatisticsCard
                              color={value.color}
                              icon={value.icon}
                              title={value.title}
                              count={userData ? apiData && apiData[index] : null}
                              loader={userData ? null : <Loader />}
                            />
                          </MDBox>
                        </Grid>
                      </>
                    );
                  })}
                </Grid>
              </MDBox>
            </Card>
          </Grid>

          <Grid item xs={12}>
            <Card>
              <MDBox display="flex" alignItems="center" width="90%" className="chart-heading">
                <MDBox
                  borderRadius="lg"
                  display="flex"
                  alignItems="center"
                  justifyContent="space-between"
                  p={3}
                  ml={0}
                >
                  <DatePicker
                    dateFormat="MM/dd/yyy"
                    selectsRange={true}
                    startDate={startDate}
                    value={rangeFormat === "Invalid date" ? rangeDate : rangeFormat}
                    endDate={endDate}
                    // readOnly={true}
                    onChange={(update) => {
                      console.log(update);
                      setDateRange(update);
                    }}
                    onKeyDown={(e) => {
                      e.preventDefault();
                    }}
                    isClearable={false}
                    className="selecting-date"
                  >
                    <div className="datepicker-button ">
                      <button
                        className="choosedate-button"
                        onClick={() => setDateRange([null, null])}
                      >
                        clear
                      </button>
                    </div>
                  </DatePicker>
                  {configsButton}
                  {!isLoading && (
                    <MDButton
                      variant="gradient"
                      color="info"
                      onClick={userClicks}
                      className="submit"
                    >
                      &nbsp;Submit
                    </MDButton>
                  )}
                  {isLoading && (
                    <MDButton
                      variant="gradient"
                      color="info"
                      onClick={userClicks}
                      disabled
                      className="submit"
                    >
                      <Icon fontSize="small" color="inherit">
                        loop
                      </Icon>
                      &nbsp;Loading...
                    </MDButton>
                  )}
                </MDBox>
              </MDBox>

              <MDBox
                display="flex"
                flexDirection="row"
                justifyContent="space-between"
                gap="5px"
                ml={2}
                mt={0}
                mr={2}
              >
                <MDBox display="flex" flexDirection="row" gap="5px">
                  <MDTypography
                    component="p"
                    fontSize="medium"
                    variant="button"
                    color="text"
                    display="flex"
                  >
                    &nbsp;Start date -
                    <MDTypography
                      component="span"
                      variant="button"
                      fontSize="medium"
                      fontWeight="bold"
                      color="success"
                    >
                      {moment(startDate).format("MM-DD-YYYY") === "Invalid date"
                        ? fdate && moment(fdate).format("MM-DD-YYYY")
                        : moment(startDate).format("MM-DD-YYYY")}
                    </MDTypography>
                  </MDTypography>

                  <MDTypography
                    component="p"
                    fontSize="medium"
                    variant="button"
                    color="text"
                    display="flex"
                  >
                    &nbsp;&nbsp;End date -
                    <MDTypography
                      component="span"
                      variant="button"
                      fontSize="medium"
                      fontWeight="bold"
                      color="success"
                    >
                      {moment(endDate ? endDate : startDate).format("MM-DD-YYYY") === "Invalid date"
                        ? ldate && moment(ldate).format("MM-DD-YYYY")
                        : moment(endDate ? endDate : startDate).format("MM-DD-YYYY")}
                    </MDTypography>
                  </MDTypography>
                </MDBox>
              </MDBox>

              <MDBox
                display="flex"
                textAlign="center"
                flexDirection="row"
                justifyContent="center"
                gap="5px"
                ml={18}
                mt={2.5}
                mr={0}
              ></MDBox>

              {valueLoader && (
                <MDBox display="flex" justifyContent="center" alignItems="center" pb={30} pt={30}>
                  <Loader />
                </MDBox>
              )}
              {!valueLoader && (
                <MDBox>
                  <MDBox pt={3} mb={5}>
                    <DataTable
                      table={{ columns: pColumns, rows: pRows }}
                      isSorted={false}
                      entriesPerPage={false}
                      showTotalEntries={false}
                      noEndBorder
                      topHeader={true}
                      headingOne="Engagement levels on each content"
                      headingTwo="Unique users Engagement levels on each content "
                    />
                  </MDBox>
                  <MDBox pt={3}>
                    <DataTable
                      table={{ columns: mColumns, rows: mRows }}
                      isSorted={false}
                      entriesPerPage={false}
                      showTotalEntries={false}
                      noEndBorder
                      topHeader={true}
                      headingOne="Unique users Engagement levels on each content (Except Holiday Content)"
                      headingTwo="Unique users Engagement(With & Without Branding) levels on each content "
                    />
                  </MDBox>
                </MDBox>
              )}
            </Card>
          </Grid>
        </Grid>
      </MDBox>
    </DashboardLayout>
  );
}

export default Tables;
