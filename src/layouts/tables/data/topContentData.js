/* eslint-disable react/prop-types */
/* eslint-disable react/function-component-definition */
/**
=========================================================
* Material Dashboard 2 React - v2.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// @mui material components
import Icon from "@mui/material/Icon";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";


export default function data(row,type) {
  console.log(row?.data);
  const Project = ({ name }) => (
    <MDBox display="flex" alignItems="center" lineHeight={1}>
      <MDTypography display="block" variant="button" fontWeight="medium" ml={0} lineHeight={1}>
        {name}
      </MDTypography>
    </MDBox>
  );

  // const Progress = ({ color, value }) => (
  //   <MDBox display="flex" alignItems="center">
  //     <MDTypography variant="caption" color="text" fontWeight="medium">
  //       {value}%
  //     </MDTypography>
  //     <MDBox ml={0.5} width="9rem">
  //       <MDProgress variant="gradient" color={color} value={value} />
  //     </MDBox>
  //   </MDBox>
  // );

  return { 
    columns: [
      { Header: "Number", accessor: "project", width: "15%", align: "left" },
      { Header: "Meme Slug", accessor: "Share", align: "left" },
      { Header: "Meme Count", accessor: "Scheduled", align: "center" },
      
    ],

    
    rows: [ 
      
      {
        project: <Project name="1"/>,
        Share: (
          <MDTypography component="a" href="#" variant="button" color="text" fontWeight="medium">
            {row?.data?.heading.split(" ")[0]==="Meme"&&row?.data?.item[0]?.meme_slug}
            {row?.data?.heading.split(" ")[0]==="Article"&&row?.data?.item[0]?.article_slug}
            {type==="videomeme"&&row?.data?.item[0]?.meme_slug}
            {/* {row?.data?.item[0]?.meme_slug} */}
          </MDTypography>
        ),
        Scheduled: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {/* {row?.data?.item[0]?.meme_count} */}
            {row?.data?.heading.split(" ")[0]==="Meme"&&row?.data?.item[0]?.meme_count}
            {row?.data?.heading.split(" ")[0]==="Article"&&row?.data?.item[0]?.article_count}
            {type==="videomeme"&&row?.data?.item[0]?.meme_slug}
          </MDTypography>
        ),
        
      },
      {
        project: <Project name="2" />,
        Share: (
          <MDTypography component="a" href="#" variant="button" color="text" fontWeight="medium">
            {row?.data?.item[1]?.meme_slug}
          </MDTypography>
        ),
        Scheduled: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {row?.data?.item[1]?.meme_count}
          </MDTypography>
        ),
        
      },
      {
        project: <Project name="3" />,
        Share: (
          <MDTypography component="a" href="#" variant="button" color="text" fontWeight="medium">
           {row?.data?.item[2]?.meme_slug}
          </MDTypography>
        ),
        Scheduled: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {row?.data?.item[2]?.meme_count}
          </MDTypography>
        ),
        
      },
      {
        project: <Project name="4" />,
        Share: (
          <MDTypography component="a" href="#" variant="button" color="text" fontWeight="medium">
            {row?.data?.item[3]?.meme_slug}
          </MDTypography>
        ),
        Scheduled: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
           {row?.data?.item[3]?.meme_count}
          </MDTypography>
        ),
        
      },
      {
        project: <Project name="5" />,
        Share: (
          <MDTypography component="a" href="#" variant="button" color="text" fontWeight="medium">
            {row?.data?.item[4]?.meme_slug}
          </MDTypography>
        ),
        Scheduled: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
           {row?.data?.item[4]?.meme_count}
          </MDTypography>
        ),
        
      },
      {
        project: <Project name="6" />,
        Share: (
          <MDTypography component="a" href="#" variant="button" color="text" fontWeight="medium">
            {row?.data?.item[5]?.meme_slug}
          </MDTypography>
        ),
        Scheduled: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {row?.data?.item[5]?.meme_count}
          </MDTypography>
        ),
        
      },
      {
        project: <Project name="7" />,
        Share: (
          <MDTypography component="a" href="#" variant="button" color="text" fontWeight="medium">
            {row?.data?.item[6]?.meme_slug}
          </MDTypography>
        ),
        Scheduled: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
           {row?.data?.item[6]?.meme_count}
          </MDTypography>
        ),
        
      },
      {
        project: <Project name="8" />,
        Share: (
          <MDTypography component="a" href="#" variant="button" color="text" fontWeight="medium">
            {row?.data?.item[7]?.meme_slug}
          </MDTypography>
        ),
        Scheduled: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {row?.data?.item[7]?.meme_count}
          </MDTypography>
        ),
        
      },
      {
        project: <Project name="9" />,
        Share: (
          <MDTypography component="a" href="#" variant="button" color="text" fontWeight="medium">
           {row?.data?.item[8]?.meme_slug}
          </MDTypography>
        ),
        Scheduled: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {row?.data?.item[8]?.meme_count}
          </MDTypography>
        ),
        
      },
      {
        project: <Project name="10" />,
        Share: (
          <MDTypography component="a" href="#" variant="button" color="text" fontWeight="medium">
            {row?.data?.item[9]?.meme_slug}
          </MDTypography>
        ),
        Scheduled: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {row?.data?.item[9]?.meme_count}
          </MDTypography>
        ),
        
      },
    ],
  };
}


