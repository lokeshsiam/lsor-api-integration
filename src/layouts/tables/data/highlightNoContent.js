/* eslint-disable react/prop-types */
/* eslint-disable react/function-component-definition */
/**
=========================================================
* Material Dashboard 2 React - v2.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// @mui material components
import Icon from "@mui/material/Icon";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";

export default function data(row) {
  console.log(row?.data);
  const Project = ({ name }) => (
    <MDBox display="flex" alignItems="center" lineHeight={1}>
      <MDTypography display="block" variant="button" fontWeight="medium" ml={0} lineHeight={1}>
        {name}
      </MDTypography>
    </MDBox>
  );

  // const Progress = ({ color, value }) => (
  //   <MDBox display="flex" alignItems="center">
  //     <MDTypography variant="caption" color="text" fontWeight="medium">
  //       {value}%
  //     </MDTypography>
  //     <MDBox ml={0.5} width="9rem">
  //       <MDProgress variant="gradient" color={color} value={value} />
  //     </MDBox>
  //   </MDBox>
  // );

  return {
    columns: [
      { Header: "Content", accessor: "project", width: "15%", align: "left" },
      { Header: "Share ", accessor: "Share", align: "left" },
      { Header: "Scheduled", accessor: "Scheduled", align: "center" },
      { Header: "Download/ Embeded", accessor: "Download", align: "center" },
    ],

    
    rows: [
      {
        project: <Project name="Meme" />,
        Share: (
          <MDTypography component="a" href="#" variant="button" color="text" fontWeight="medium">
            {row?.shared_memes_count}
          </MDTypography>
        ),
        Scheduled: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {row?.scheduled_memes_count}
          </MDTypography>
        ),
        Download: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {row?.download_memes_count}{" "}
          </MDTypography>
        ),
        
      },
      {
        project: <Project name="Videomeme" />,
        Share: (
          <MDTypography component="a" href="#" variant="button" color="text" fontWeight="medium">
            {row?.shared_video_memes_count}{" "}
          </MDTypography>
        ),
        Scheduled: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {row?.scheduled_video_memes_count}{" "}
          </MDTypography>
        ),
        Download: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {row?.download_video_memes_count}{" "}
          </MDTypography>
        ),
       
      },
      {
        project: <Project name="Article" />,
        Share: (
          <MDTypography component="a" href="#" variant="button" color="text" fontWeight="medium">
            {row?.shared_posts_count}{" "}
          </MDTypography>
        ),
        Scheduled: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {row?.scheduled_posts_count}{" "}
          </MDTypography>
        ),
        Download: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
           {row?.embed_posts_count} 
          </MDTypography>
        ),
        
      },
    ],
  };
}
