/* eslint-disable react/prop-types */
/* eslint-disable react/function-component-definition */
/**
=========================================================
* Material Dashboard 2 React - v2.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// @mui material components
import Icon from "@mui/material/Icon";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";


export default function data(row) {
  console.log(row?.data);
  const Project = ({ name }) => (
    <MDBox display="flex" alignItems="center" lineHeight={1}>
      <MDTypography display="block" variant="button" fontWeight="medium" ml={0} lineHeight={1}>
        {name}
      </MDTypography>
    </MDBox>
  );

  // const Progress = ({ color, value }) => (
  //   <MDBox display="flex" alignItems="center">
  //     <MDTypography variant="caption" color="text" fontWeight="medium">
  //       {value}%
  //     </MDTypography>
  //     <MDBox ml={0.5} width="9rem">
  //       <MDProgress variant="gradient" color={color} value={value} />
  //     </MDBox>
  //   </MDBox>
  // );

  return {
    columns: [
      { Header: "Content", accessor: "project", width: "15%", align: "left" },
      { Header: "Share / Edited", accessor: "Share", align: "left" },
      { Header: "Scheduled/ Landing page", accessor: "Scheduled", align: "center" },
      { Header: "Download/ Embeded", accessor: "Download", align: "center" },
    ],

    rows: [
      {
        project: <Project name="User Meme"/>,
        Share: (
          <MDTypography component="a" href="#" variant="button" color="text" fontWeight="medium">
            {row?.data?.user_meme_shared_greater}
          </MDTypography>
        ),
        Scheduled: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
           {row?.data?.user_meme_scheduled_greater}
          </MDTypography>
        ),
        Download: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {row?.data?.user_meme_downloaded_greater}
          </MDTypography>
        ),

      },
      {
        project: <Project name="User Post" />,
        Share: (
          <MDTypography component="a" href="#" variant="button" color="text" fontWeight="medium">
            {row?.data?.user_post_share_greater}
          </MDTypography>
        ),
        Scheduled: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {row?.data?.user_post_schedule_greater}
          </MDTypography>
        ),
        Download: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {row?.data?.user_post_embed_greater}
          </MDTypography>
        ),
        
      },
    ],
  };
}
