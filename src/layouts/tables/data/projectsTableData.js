/* eslint-disable react/prop-types */
/* eslint-disable react/function-component-definition */
/**
=========================================================
* Material Dashboard 2 React - v2.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// @mui material components
import Icon from "@mui/material/Icon";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";


export default function data(row,userRow) {
  console.log(userRow?.data?.item?.length);
  const Project = ({ name }) => (
    <MDBox display="flex" alignItems="center" lineHeight={1}>
      <MDTypography display="block" variant="button" fontWeight="medium" ml={0} lineHeight={1}>
        {name}
      </MDTypography>
    </MDBox>
  );

  // const Progress = ({ color, value }) => (
  //   <MDBox display="flex" alignItems="center">
  //     <MDTypography variant="caption" color="text" fontWeight="medium">
  //       {value}%
  //     </MDTypography>
  //     <MDBox ml={0.5} width="9rem">
  //       <MDProgress variant="gradient" color={color} value={value} />
  //     </MDBox>
  //   </MDBox>
  // );

  return {
    columns: [
      { Header: "Content", accessor: "project", width: "15%", align: "left" },
      { Header: "Share / Edited", accessor: "Share", align: "left" },
      { Header: "Scheduled/ Landing page", accessor: "Scheduled", align: "center" },
      { Header: "Download/ Embeded", accessor: "Download", align: "center" },
      { Header: "Share / Edited", accessor: "UserShare", align: "center" },
      { Header: "Scheduled/ Landing page", accessor: "UserScheduled", align: "center" },
      { Header: "Download/ Embeded", accessor: "UserDownload", align: "center" },
    ],

    rows: [
      {
        project: <Project name="Meme/Video"/>,
        Share: (
          <MDTypography component="a" href="#" variant="button" color="text" fontWeight="medium">
            {row?.data?.item[0]?.count}
          </MDTypography>
        ),
        Scheduled: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {row?.data?.item[1]?.count}
          </MDTypography>
        ),
        Download: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {row?.data?.item[2]?.count}
          </MDTypography>
        ),
        UserShare: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {userRow?.data?.item[0]?.count}
          </MDTypography>
        ),
        UserScheduled: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {userRow?.data?.item[1]?.count}
          </MDTypography>
        ),
        UserDownload: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {userRow?.data?.item[2]?.count}
          </MDTypography>
        ),
      },
      {
        project: <Project name="Article" />,
        Share: (
          <MDTypography component="a" href="#" variant="button" color="text" fontWeight="medium">
            {row?.data?.item[3]?.count}
          </MDTypography>
        ),
        Scheduled: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {row?.data?.item[4]?.count}
          </MDTypography>
        ),
        Download: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {row?.data?.item[5]?.count}
          </MDTypography>
        ),
        UserShare: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {userRow?.data?.item[3]?.count}
          </MDTypography>
        ),
        UserScheduled: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {userRow?.data?.item[4]?.count}
          </MDTypography>
        ),
        UserDownload: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {userRow?.data?.item[5]?.count}
          </MDTypography>
        ),
      },
      {
        project: <Project name="BB" />,
        Share: (
          <MDTypography component="a" href="#" variant="button" color="text" fontWeight="medium">
            {row?.data?.item[6]?.count}
          </MDTypography>
        ),
        Scheduled: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {row?.data?.item[7]?.count}
          </MDTypography>
        ),
        Download: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            -
          </MDTypography>
        ),
        UserShare: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {userRow?.data?.item[6]?.count}
          </MDTypography>
        ),
        UserScheduled: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {userRow?.data?.item[7]?.count}
          </MDTypography>
        ),
        UserDownload: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            -
          </MDTypography>
        ),
      },
    ],
  };
}
