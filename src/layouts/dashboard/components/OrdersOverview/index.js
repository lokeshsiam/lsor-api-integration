import { useState, useEffect } from "react";
// @mui material components
import Card from "@mui/material/Card";
import Icon from "@mui/material/Icon";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Grid from "@mui/material/Grid";
// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDButton from "components/MDButton";
// Material Dashboard 2 React examples

import CircularProgress from "@mui/material/CircularProgress";

import ReportsLineChart from "examples/Charts/LineCharts/ReportsLineChart";
import ProgressLineChart from "examples/Charts/LineCharts/ProgressLineChart";
// Material Dashboard 2 React context
import { useMaterialUIController } from "context";
// Data
import data from "layouts/dashboard/components/Projects/data";
import reportsLineChartData from "layouts/dashboard/data/reportsLineChartData";
//Date-Picker
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import "../../projects.css";
import moment from "moment";

function OrdersOverview() {
  const { columns, rows } = data();
  const [menu, setMenu] = useState(null);

  const [isLoading, setLoading] = useState(false);
  const [valueLoader, setValueLoader] = useState(false);

  const { sales, tasks } = reportsLineChartData;

  const [dateRange, setDateRange] = useState([null, null]);
  const [startDate, endDate] = dateRange;

  // const [startDate, setStartDate] = useState(null);

  const [controller] = useMaterialUIController();
  const { darkMode } = controller;

  const openMenu = ({ currentTarget }) => setMenu(currentTarget);
  const closeMenu = () => setMenu(null);

  const [userData, setUserData] = useState();
  const [weeklyData, setWeeklyData] = useState();
  const [errorMessage, setErrorMessage] = useState();

  const currentDate = new Date();
  //yesterday date - end date
  const yesterdayDate = moment(currentDate).subtract(1, "days");
  const lastDate = yesterdayDate._d;
  const formatYesterdayDate = moment(lastDate).format("YYYY-MM-DD");
  //last seven day date - start date
  const lastSevenDaysDate = moment(currentDate).subtract(1, "week");
  const firstDate = lastSevenDaysDate._d;
  const formatLastDate = moment(firstDate).format("YYYY-MM-DD");

  const rangeFormat = moment(startDate).format("MM/DD/YYYY");

  const rangeDate = moment(firstDate).format("MM/DD/YYYY");

  const chartsData = () => {
    // SelectedDate();
    WeeklyDate();
  };

  useEffect(() => {
    console.log(dateRange);
    onLodeWeeklyDate();
  }, []);

  const configsButton = (
    <MDBox
      display="flex"
      justifyContent="center"
      alignItems="center"
      left="2rem"
      height="3.25rem"
      position="absolute"
      zIndex={0}
      color="dark"
      sx={{ cursor: "pointer" }}
      // onClick={handleConfiguratorOpen}
    >
      <Icon fontSize="small" color="inherit">
        event
      </Icon>
    </MDBox>
  );

  const onLodeWeeklyDate = () => {
    // if (dateRange[0]!== null) {
    console.log(startDate, endDate);
    const startData = moment(startDate).format("YYYY-MM-DD");
    const endData = moment(endDate).format("YYYY-MM-DD");
    const type = "weekly";

    const end = endDate === null ? startData : endData;

    let formData = new FormData();
    formData.append("from_date", formatLastDate);
    formData.append("to_date", formatYesterdayDate);
    formData.append("type", type);
    var myHeaders = new Headers();
    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formData,
    };

    const fetchWeeklyData = async () => {
      setValueLoader(true);
      // setLoading(true);
      const response = await fetch(
        "https://ic-dev.lightersideofrealestate.com/ic-scripts_sync_codes/weekly_reports/api_dau_report.php",
        requestOptions
      );
      const data = await response.json();
      setWeeklyData({ data });
      setValueLoader(false);
      setLoading(false);
      console.log(dateRange[0]);
      console.log(startData);
      console.log(endData);
      if (data) {
        console.log(weeklyData);
      }
    };

    fetchWeeklyData();
    // }
  };

  const WeeklyDate = () => {
    console.log(dateRange[0] !== null);
    if (startDate) {
      console.log("hi");
      console.log(startDate, endDate);
      const startData = moment(startDate).format("YYYY-MM-DD");
      const endData = moment(endDate).format("YYYY-MM-DD");
      const type = "weekly";

      const end = endDate === null ? startData : endData;

      let formData = new FormData();
      formData.append("from_date", startData);
      formData.append("to_date", end);
      formData.append("type", type);
      var myHeaders = new Headers();
      const requestOptions = {
        method: "POST",
        headers: myHeaders,
        body: formData,
      };

      const fetchWeeklyData = async () => {
        setLoading(true);
        const response = await fetch(
          "https://ic-dev.lightersideofrealestate.com/ic-scripts_sync_codes/weekly_reports/api_dau_report.php",
          requestOptions
        );
        const data = await response.json();
        setWeeklyData({ data });
        setLoading(false);
        console.log(dateRange[0]);
        console.log(startData);
        console.log(endData);
        if (data) {
          console.log(weeklyData);
        }
      };

      fetchWeeklyData();
    }
  };

  // useEffect(() => {
  //   fetchData();
  //   console.log(userData);
  // }, []);

  const renderMenu = (
    <Menu
      id="simple-menu"
      anchorEl={menu}
      anchorOrigin={{
        vertical: "top",
        horizontal: "left",
      }}
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={Boolean(menu)}
      onClose={closeMenu}
    >
      <MenuItem onClick={closeMenu}>Action</MenuItem>
      <MenuItem onClick={closeMenu}>Another action</MenuItem>
      <MenuItem onClick={closeMenu}>Something else</MenuItem>
    </Menu>
  );
  // console.log(dateRange[0]);

  return (
    <Card>
      <MDBox display="flex" justifyContent="space-between" alignItems="center" p={3}>
        <MDBox>
          <MDTypography variant="h6" gutterBottom>
            Weekly Active Users
          </MDTypography>
        </MDBox>

        {renderMenu}
      </MDBox>

      <MDBox display="flex" alignItems="center" width="98%" className="chart-heading">
        <MDBox
          borderRadius="lg"
          display="flex"
          alignItems="center"
          justifyContent="space-between"
          p={3}
          mb={3}
        >
          <DatePicker
            dateFormat="MM/DD/YYYY"
            // readOnly={true}
            selectsRange={true}
            startDate={startDate}
            value={rangeFormat === "Invalid date" ? rangeDate : rangeFormat}
            endDate={startDate}
            selected={startDate}
            onChange={(update) => {
              console.log(update);
              setDateRange(update);
            }}
            // onChange={(date) => {
            //   setStartDate(date)
            //   // console.log(dateRange[0])
            // }}
            onKeyDown={(e) => {
              e.preventDefault();
            }}
            isClearable={false}
            className="selecting-date"
          >
            <div className="datepicker-button ">
              <button className="choosedate-button" onClick={() => setDateRange([null, null])}>
                clear
              </button>
            </div>
          </DatePicker>

          {configsButton}
          {!isLoading && (
            <MDButton variant="gradient" color="info" onClick={chartsData} className="submit">
              &nbsp;Submit
            </MDButton>
          )}
          {isLoading && (
            <MDButton
              variant="gradient"
              color="info"
              onClick={chartsData}
              disabled
              className="submit"
            >
              <Icon fontSize="small" color="inherit">
                loop
              </Icon>
              &nbsp;Loading...
            </MDButton>
          )}
        </MDBox>
      </MDBox>

      <MDBox>
        {valueLoader && (
          <MDBox display="flex" justifyContent="center" alignItems="center" pb={20} pt={14}>
            <CircularProgress size="1.5rem" color="inherit" />
          </MDBox>
        )}
        {!valueLoader && (
          <Grid container spacing={3}>
            <Grid item xs={12} md={12} lg={7} className="graph">
              <MDBox mb={3}>
                <ProgressLineChart color="success" chart={weeklyData} />
              </MDBox>
            </Grid>
            <Grid item xs={12} md={12} lg={5}>
              <MDBox
                display="flex"
                flexDirection="column"
                alignItems="center"
                justifyContent="center"
                className="date-data"
                gap="5px"
                mt={0}
                // ml={5}
                pb={3}
                pr={2}
                // width="100%"
                height="100%"
                width="100%"
              >
                <MDBox display="flex" flexDirection="column" alignItems="start">
                  {weeklyData &&
                    weeklyData.data.item.map((value, index) => {
                      return (
                        <MDBox display="flex" alignItems="end" justifyContent="left">
                          {/* <MDTypography variant="button" fontSize="medium" color="text">
                     {"Day"+(index+1)+"-"+value.dau_daily_range+"-"+value.dau_daily_count}
                   </MDTypography> */}

                          <MDBox
                            display="flex"
                            flexDirection="column"
                            alignItems="start"
                            justifyContent="flex-start"
                            minWidth="40px"
                            maxWidth="100px"
                          >
                            <MDTypography variant="button" fontSize="medium" color="text">
                              {"W" + (index + 1)}
                            </MDTypography>
                          </MDBox>

                          <MDBox
                            display="flex"
                            flexDirection="column"
                            minWidth="10px"
                            maxWidth="10px"
                          >
                            <MDTypography variant="button" fontSize="medium" color="text">
                              -
                            </MDTypography>
                          </MDBox>

                          <MDBox
                            display="flex"
                            flexDirection="column"
                            minWidth="20px"
                            maxWidth="300px"
                          >
                            <MDTypography variant="button" fontSize="medium" color="text">
                              {value.dau_weekly_range}&nbsp;
                            </MDTypography>
                          </MDBox>

                          <MDBox
                            display="flex"
                            flexDirection="column"
                            alignItems="end"
                            minWidth="20px"
                            maxWidth="20px"
                          >
                            <MDTypography variant="button" fontSize="medium" color="text">
                              -&nbsp;
                            </MDTypography>
                          </MDBox>

                          <MDBox
                            display="flex"
                            flexDirection="column"
                            textAlign="end"
                            justifyContent="flex-end"
                            minWidth="20px"
                            maxWidth="200px"
                          >
                            <MDTypography
                              variant="button"
                              fontSize="medium"
                              color="text"
                              fontWeight="bold"
                            >
                              <span className="color-text">{value.dau_weekly_count}</span>
                            </MDTypography>
                          </MDBox>
                        </MDBox>
                      );
                    })}
                </MDBox>

                {/* <MDBox display="flex" flexDirection="column">
                <MDTypography variant="button" fontSize="medium" color="text">
                    W1-
                  </MDTypography>
                  <MDTypography variant="button" fontSize="medium" color="text">
                    W2-
                  </MDTypography>
                  <MDTypography variant="button" fontSize="medium" color="text">
                    W3-
                  </MDTypography>
                  <MDTypography variant="button" fontSize="medium" color="text">
                    W4-
                  </MDTypography>
                </MDBox> */}
                {/* <MDBox display="flex" flexDirection="column">
                  <MDTypography variant="button" fontSize="medium" color="text">
                    {weeklyData && weeklyData.data.item[0].dau_weekly_range}
                  </MDTypography>
                  <MDTypography variant="button" fontSize="medium" color="text">
                    {weeklyData && weeklyData.data.item[1].dau_weekly_range}
                  </MDTypography>
                  <MDTypography variant="button" fontSize="medium" color="text">
                    {weeklyData && weeklyData.data.item[2].dau_weekly_range}
                  </MDTypography>
                  <MDTypography variant="button" fontSize="medium" color="text">
                    {weeklyData && weeklyData.data.item[3].dau_weekly_range}
                  </MDTypography>
                </MDBox> */}
                {/* <MDBox display="flex" flexDirection="column" alignItems="end">
                  <MDTypography variant="button" fontSize="medium" color="text">
                    -
                  </MDTypography>
                  <MDTypography variant="button" fontSize="medium" color="text">
                    -
                  </MDTypography>
                  <MDTypography variant="button" fontSize="medium" color="text">
                    -
                  </MDTypography>
                  <MDTypography variant="button" fontSize="medium" color="text">
                    -
                  </MDTypography>
                </MDBox> */}
                {/* <MDBox display="flex" flexDirection="column" alignItems="end">
                  <MDTypography variant="button" fontSize="medium" fontWeight="bold" color="text">
                    <span className="color-text">
                      {weeklyData && weeklyData.data.item[0].dau_weekly_count}{" "}
                    </span>
                  </MDTypography>
                  <MDTypography variant="button" fontSize="medium"  color="text">
                    <span className="color-text">
                      {weeklyData && weeklyData.data.item[1].dau_weekly_count}{" "}
                    </span>
                  </MDTypography>
                  <MDTypography variant="button" fontSize="medium"  color="text">
                    <span className="color-text">
                      {weeklyData && weeklyData.data.item[2].dau_weekly_count}{" "}
                    </span>
                  </MDTypography>
                  <MDTypography variant="button" fontSize="medium"  color="text">
                    <span className="color-text">
                      {weeklyData && weeklyData.data.item[3].dau_weekly_count}{" "}
                    </span>
                  </MDTypography>
                </MDBox> */}
              </MDBox>
            </Grid>
          </Grid>
        )}
      </MDBox>
    </Card>
  );
}

export default OrdersOverview;
