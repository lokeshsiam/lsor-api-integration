import { useState, useEffect } from "react";
// @mui material components
import Card from "@mui/material/Card";
import Icon from "@mui/material/Icon";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDButton from "components/MDButton";
import Header from "components/Header/Header";

// Material Dashboard 2 React examples
import ComplexStatisticsCard from "examples/Cards/StatisticsCards/ComplexStatisticsCard";
// Material Dashboard 2 React context
import { useMaterialUIController } from "context";

import CircularProgress from "@mui/material/CircularProgress";

// Data
import moment from "moment";
//Date-Picker
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import "../../projects.css";

function NoActiveUsers() {
  const [menu, setMenu] = useState(null);
  // const startData = moment(startDate).format("YYYY-MM-DD");
  const [userData, setUserData] = useState();

  const [thirtyCount, setThirtyCount] = useState();
  const [sixtyCount, setSixtyCount] = useState();
  const [nintyCount, setNintyCount] = useState();

  const [valueLoader, setValueLoader] = useState(false);

  const [isLoading, setLoading] = useState(false);
  const [dateRange, setDateRange] = useState([null, null]);
  const [startDate, endDate] = dateRange;

  const [controller] = useMaterialUIController();
  const closeMenu = () => setMenu(null);


  const onLoadNoActiveUser = (value1, value2) => {
    // alert(dateRange[1])
    // if (dateRange[0] !== null) {
    const thirtyStartValue = "30";
    const thirtyEndValue = "60";
    const type = "count";

    let formData = new FormData();
    formData.append("start", value1);
    formData.append("end", value2);
    formData.append("type", type);
    var myHeaders = new Headers();
    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formData,
    };
    const fetchData = async () => {
      setValueLoader(true);
      // setLoading(true);
      const response = await fetch(
        "https://ic-dev.lightersideofrealestate.com/ic-scripts_sync_codes/weekly_reports/no_active_user.php",
        requestOptions
      );
      const data = await response.json();
      if (value1 === 30) {
        setThirtyCount({ data });
      } else if (value1 === 60) {
        setSixtyCount({ data });
      } else {
        setNintyCount({ data });
      }
      setLoading(false);
      setValueLoader(false);

      console.log(dateRange[0]);
      console.log(thirtyStartValue);
      // console.log(end);
      if (data) {
        console.log(data);
      }
    };

    fetchData();
    // }
  };

  useEffect(() => {
    onLoadNoActiveUser(30, 60);
    onLoadNoActiveUser(60, 90);
    onLoadNoActiveUser(90, "");
  }, []);

  const renderMenu = (
    <Menu
      id="simple-menu"
      anchorEl={menu}
      anchorOrigin={{
        vertical: "top",
        horizontal: "left",
      }}
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={Boolean(menu)}
      onClose={closeMenu}
    >
      <MenuItem onClick={closeMenu}>Action</MenuItem>
      <MenuItem onClick={closeMenu}>Another action</MenuItem>
      <MenuItem onClick={closeMenu}>Something else</MenuItem>
    </Menu>
  );

  return (
    <Card>
      <MDBox display="flex" justifyContent="space-between" alignItems="center" p={3}>
        <MDBox>
          <MDTypography variant="h6" gutterBottom>
            No Active Users
          </MDTypography>
        </MDBox>

        {renderMenu}
      </MDBox>
      

      <MDBox mb={6.5} sx={{ textAlign: "start" }}>
      {valueLoader && (
          <MDBox display="flex" justifyContent="center" alignItems="center" pb={6} pt={3} >
            <CircularProgress size="1.5rem" color="inherit" />
          </MDBox>
        )}
        {!valueLoader && (
           <MDBox
           display="flex"
           flexDirection="row"
           alignItems="center"
           justifyContent="center"
           className="date-data"
           gap="5px"
           mb={1.8}
           height="100%"
         >
           <MDBox display="flex" flexDirection="column" gap="5px">
             <MDTypography variant="button" fontSize="medium" color="text">
             30 to 60 Days
             </MDTypography>
             <MDTypography variant="button" fontSize="medium" color="text">
             60 to 90 Days
             </MDTypography>
             <MDTypography variant="button" fontSize="medium" color="text">
             90+ Days
             </MDTypography>
           </MDBox>

           <MDBox display="flex" flexDirection="column"  gap="5px">
             <MDTypography variant="button" fontSize="medium" color="text">
               -
             </MDTypography>
             <MDTypography variant="button" fontSize="medium" color="text">
               -
             </MDTypography>
             <MDTypography variant="button" fontSize="medium" color="text">
               -
             </MDTypography>
           </MDBox>

           <MDBox display="flex" flexDirection="column" alignItems="end" gap="5px">
             <MDTypography variant="button" fontSize="medium" fontWeight="bold" color="text">
               <span className="color-text">
               {thirtyCount?.data?.count}
               </span>
             </MDTypography>
             <MDTypography variant="button" fontSize="medium" fontWeight="bold" color="text">
               <span className="color-text">
               {sixtyCount?.data?.count}
               </span>
             </MDTypography>
             <MDTypography variant="button" fontSize="medium" fontWeight="bold" color="text">
               <span className="color-text">
               {nintyCount?.data?.count}
               </span>
             </MDTypography>
           </MDBox>
         </MDBox>
        // <Grid container direction="row" justifyContent="center">
        //   <Grid item sm={6} md={6} lg={8} textAlign="end">
        //     <MDTypography variant="button" fontWeight="light" color="text" fontSize="large">
        //       30 to 60 Days&nbsp;&nbsp;=
        //     </MDTypography>
        //   </Grid>
        //   <Grid item xs={12} sm={2} md={1} lg={3} textAlign="end">
        //     <MDTypography
        //       component="span"
        //       variant="button"
        //       fontSize="large"
        //       fontWeight="bold"
        //       color="success"
        //     >
        //       &nbsp;{thirtyCount?.data?.count}
        //     </MDTypography>
        //   </Grid>

        //   <Grid item sm={6} md={6} lg={8} textAlign="end">
        //   <MDTypography variant="button" fontWeight="light" color="text" fontSize="large">
        //       60 to 90 Days&nbsp;&nbsp;=
        //     </MDTypography>
        //   </Grid>
        //   <Grid item xs={12} sm={2} md={1} lg={3} textAlign="end">
        //     <MDTypography
        //       component="span"
        //       variant="button"
        //       fontSize="large"
        //       fontWeight="bold"
        //       color="success"
        //     >
        //       &nbsp;{sixtyCount?.data?.count}
        //     </MDTypography>
        //   </Grid>

        //   <Grid item sm={6} md={6} lg={8} textAlign="end">
        //   <MDTypography variant="button" fontWeight="light" color="text" fontSize="large">
        //       90+ Days &nbsp;&nbsp;=
        //     </MDTypography>
        //   </Grid>
        //   <Grid item xs={12} sm={2} md={1} lg={3} textAlign="end">
        //     <MDTypography
        //       component="span"
        //       variant="button"
        //       fontSize="large"
        //       fontWeight="bold"
        //       color="success"
        //     >
        //       &nbsp;{nintyCount?.data?.count}
        //     </MDTypography>
        //   </Grid>
        // </Grid>
        )}
      </MDBox>
    </Card>
  );
}

export default NoActiveUsers;
