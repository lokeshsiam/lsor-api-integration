import { useState, useEffect } from "react";
// @mui material components
import Card from "@mui/material/Card";
import Icon from "@mui/material/Icon";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Grid from "@mui/material/Grid";
// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDButton from "components/MDButton";
// Material Dashboard 2 React examples
import ComplexStatisticsCard from "examples/Cards/StatisticsCards/ComplexStatisticsCard";
// Material Dashboard 2 React context
import { useMaterialUIController } from "context";
import CircularProgress from "@mui/material/CircularProgress";
// import Header from "components/Header/Header";

// Data
import moment from "moment";
//Date-Picker
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import "../../projects.css";

function WeeklyUsers() {
  const [menu, setMenu] = useState(null);
  // const startData = moment(startDate).format("YYYY-MM-DD");
  const [userData, setUserData] = useState();

  const [isLoading, setLoading] = useState(false);

  const [dateRange, setDateRange] = useState([null, null]);
  const [startDate, endDate] = dateRange;

  const [controller] = useMaterialUIController();
  const closeMenu = () => setMenu(null);

  const currentDate = new Date();
  //yesterday date - end date
  const yesterdayDate = moment(currentDate).subtract(1, "days");
  const lastDate = yesterdayDate._d;
  const formatYesterdayDate = moment(lastDate).format("YYYY-MM-DD");
  //last seven day date - start date
  const lastSevenDaysDate = moment(currentDate).subtract(1, "week");
  const firstDate = lastSevenDaysDate._d;
  const formatLastDate = moment(firstDate).format("YYYY-MM-DD");

  //loader//
  const LoadingData = () => {
    return (
      <MDBox sx={{ display: "flex", justifyContent: "center" }} pt={1} pr={2} pb={3.1}>
        <CircularProgress size="1.5rem" color="inherit" />
      </MDBox>
    );
  };

  const rangeFormat =
    endDate === null
      ? moment(startDate).format("MM/DD/YYYY")
      : moment(startDate).format("MM/DD/YYYY") +
        " " +
        "-" +
        " " +
        moment(endDate).format("MM/DD/YYYY");

  const rangeDate =
    moment(firstDate).format("MM/DD/YYYY") +
    " " +
    "-" +
    " " +
    moment(lastDate).format("MM/DD/YYYY");

  useEffect(() => {
    console.log(yesterdayDate);
    console.log(formatYesterdayDate);
    console.log(lastSevenDaysDate._d);
    console.log(formatLastDate);
    console.log(rangeDate);

    onLoadSelectedDate();
  }, []);

  const configsButton = (
    <MDBox
      display="flex"
      justifyContent="center"
      // flexDirection="column"
      alignItems="center"
      left="2.5rem"
      height="3.25rem"
      position="absolute"
      zIndex={0}
      color="dark"
      sx={{ cursor: "pointer" }}
      // onClick={() => <DatePicker/>}
    >
      <Icon fontSize="small" color="inherit">
        event
      </Icon>
    </MDBox>
  );

  const onLoadSelectedDate = () => {
    // alert(dateRange[1])
    // if (dateRange[0] !== null) {
    console.log(startDate, endDate);

    const startData = moment(startDate).format("MM/DD/YYYY");
    const endData = moment(endDate).format("MM/DD/YYYY");

    // const end = endDate === null ? formatLastDate : formatYesterdayDate ;
    // alert(end);
    const type = "weekly";
    // if(dateRange[1] !== null) {
    //   endData = startData ;
    // }else {
    //   endData = endData;
    // }

    const currentDate = new Date();
    //yesterday date - end date
    const yesterdayDate = moment(currentDate).subtract(1, "days");
    const lastDate = yesterdayDate._d;
    const formatYesterdayDate = moment(lastDate).format("YYYY-MM-DD");
    //last seven day date - start date
    const lastSevenDaysDate = moment(currentDate).subtract(1, "week");
    const firstDate = lastSevenDaysDate._d;
    const formatLastDate = moment(firstDate).format("YYYY-MM-DD");

    let formData = new FormData();
    formData.append("from_date", formatLastDate);
    formData.append("to_date", formatYesterdayDate);
    formData.append("type", type);
    var myHeaders = new Headers();
    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formData,
    };
    const fetchData = async () => {
      // setLoading(true);
      const response = await fetch(
        "https://ic-dev.lightersideofrealestate.com/ic-scripts_sync_codes/weekly_reports/api_dau_report.php",
        requestOptions
      );
      const data = await response.json();
      setUserData({ data });
      setLoading(false);
      console.log(dateRange[0]);
      console.log(startData);
      // console.log(end);
      if (data) {
        console.log(data);
      }
    };

    fetchData();
    // }
  };

  const SelectedDate = () => {
    // alert(dateRange[1])
    if (dateRange[0] !== null) {
      console.log(startDate, endDate);
      const apiTime = "23:59:59";

      const startData = moment(startDate).format("YYYY-MM-DD");
      const endData = moment(endDate).format("YYYY-MM-DD");

      const end = endDate === null ? startData : endData;
      // alert(end);
      const type = "weekly";
      // if(dateRange[1] !== null) {
      //   endData = startData ;
      // }else {
      //   endData = endData;
      // }

      let formData = new FormData();
      formData.append("from_date", startData);
      formData.append("to_date", end + " " + apiTime);
      formData.append("type", type);
      var myHeaders = new Headers();
      const requestOptions = {
        method: "POST",
        headers: myHeaders,
        body: formData,
      };
      const fetchData = async () => {
        setLoading(true);
        const response = await fetch(
          "https://ic-dev.lightersideofrealestate.com/ic-scripts_sync_codes/weekly_reports/api_dau_report.php",
          requestOptions
        );
        const data = await response.json();
        setUserData({ data });
        setLoading(false);
        console.log(dateRange[0]);
        console.log(startData);
        console.log(end);
        // console.log(userData && userData.data.dau_monthly_count);
        if (data) {
          console.log(data);
        }
      };

      fetchData();
    }
  };

  const renderMenu = (
    <Menu
      id="simple-menu"
      anchorEl={menu}
      anchorOrigin={{
        vertical: "top",
        horizontal: "left",
      }}
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={Boolean(menu)}
      onClose={closeMenu}
    >
      <MenuItem onClick={closeMenu}>Action</MenuItem>
      <MenuItem onClick={closeMenu}>Another action</MenuItem>
      <MenuItem onClick={closeMenu}>Something else</MenuItem>
    </Menu>
  );

  return (
    <Card>
      <MDBox display="flex" justifyContent="space-between" alignItems="center" p={3}>
        <MDBox>
          <MDTypography variant="h6" gutterBottom>
            Active Users
          </MDTypography>
        </MDBox>

        {renderMenu}
      </MDBox>
      {/* <Header header="engae" /> */}

      <MDBox
        borderRadius="lg"
        display="flex"
        alignItems="center"
        justifyContent="space-between"
        p={3}
        ml={3}
      >
        <Grid container spacing={2}>
          <Grid item xs={12} sm={12} md={6} lg={6} py={2} >
            <MDBox borderRadius="lg" display="flex" alignItems="center" ml={-2}>
              <DatePicker
                dateFormat="MM/dd/yyy"
                selectsRange={true}
                startDate={startDate}
                value={rangeFormat === "Invalid date" ? rangeDate : rangeFormat}
                endDate={endDate}
                // readOnly={true}
                onChange={(update) => {
                  console.log(update);
                  setDateRange(update);
                }}
                onKeyDown={(e) => {
                  e.preventDefault();
                }}
                isClearable={false}
                className="selecting-date"
              >
                <div className="datepicker-button ">
                  <button className="choosedate-button" onClick={() => setDateRange([null, null])}>
                    clear
                  </button>
                </div>
              </DatePicker>
              {configsButton}

              {!isLoading && (
                <MDButton
                  variant="gradient"
                  color="info"
                  onClick={SelectedDate}
                  className="weeklySubmit"
                >
                  &nbsp;Submit
                </MDButton>
              )}
              {isLoading && (
                <MDButton
                  variant="gradient"
                  color="info"
                  onClick={SelectedDate}
                  disabled
                  className="submit"
                >
                  <Icon fontSize="small" color="inherit">
                    loop
                  </Icon>
                  &nbsp;Loading...
                </MDButton>
              )}
            </MDBox>
          </Grid>

          <Grid item xs={12} sm={12} md={6} lg={6}>
            <MDBox mb={1.6}>
              <ComplexStatisticsCard
                icon="leaderboard"
                title="Active Users"
                count={userData ? userData.data.dau_monthly_count : null}
                loader={userData ? null : <LoadingData />}
                // amountStart= {startDate}
                percentage={{
                  color: "success",
                  amountStart:
                    moment(startDate).format("MM-DD-YYYY") === "Invalid date"
                      ? moment(formatLastDate).format("MM-DD-YYYY")
                      : moment(startDate).format("MM-DD-YYYY"),
                  amountEnd:
                    moment(endDate ? endDate : startDate).format("MM-DD-YYYY") === "Invalid date"
                      ? moment(formatYesterdayDate).format("MM-DD-YYYY")
                      : moment(endDate ? endDate : startDate).format("MM-DD-YYYY"),
                  label1: "Start date -",
                  label2: "End date -",
                }}
              />
            </MDBox>
          </Grid>
        </Grid>
      </MDBox>
    </Card>
  );
}

export default WeeklyUsers;
