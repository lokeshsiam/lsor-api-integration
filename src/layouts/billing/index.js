/**
=========================================================
* Material Dashboard 2 React - v2.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/
import React, { useState, useEffect } from "react";
// @mui material components
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Icon from "@mui/material/Icon";

//Date-Picker
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import moment from "moment";


// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDButton from "components/MDButton";

// Material Dashboard 2 React examples
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from "examples/Footer";
import MasterCard from "examples/Cards/MasterCard";
import DefaultInfoCard from "examples/Cards/InfoCards/DefaultInfoCard";

import DataTable from "examples/Tables/DataTable";
import topContentData from "layouts/tables/data/topContentData";

// Billing page components
import PaymentMethod from "layouts/billing/components/PaymentMethod";
import Invoices from "layouts/billing/components/Invoices";
import BillingInformation from "layouts/billing/components/BillingInformation";
import Transactions from "layouts/billing/components/Transactions";

import "../../layouts/dashboard/projects.css";

function Billing() {
  const [age, setAge] = React.useState("desc");
  const [memeType, setMemeType] = useState("meme");
  const [moduleType, setModuleType] = useState("share");

  const [memeTypeData, setMemeTypeData] = useState("meme")

  const [dateRange, setDateRange] = useState([null, null]);
  const [startDate, endDate] = dateRange;

  const [onLoadTopContent,setOnLoadTopContent] = useState();
  const [topContent,setTopContent] = useState();

  const [isLoading, setLoading] = useState(false);

  const { columns: tColumns, rows: tRows } = topContentData(onLoadTopContent,memeType);

  const currentDate = new Date();
  //yesterday date - end date
  const yesterdayDate = moment(currentDate).subtract(1, "days");
  const lastDate = yesterdayDate._d;
  const formatYesterdayDate = moment(lastDate).format("YYYY-MM-DD");
  //last seven day date - start date
  const lastSevenDaysDate = moment(currentDate).subtract(1, "week");
  const firstDate = lastSevenDaysDate._d;
  const formatLastDate = moment(firstDate).format("YYYY-MM-DD");

  const handleChange = (event) => {
    setAge(event.target.value);
  };

  const memeChange = (event) => {
    setMemeType(event.target.value);
  };

  const moduleChange = (event) => {
    setModuleType(event.target.value);
  };

  const onClickButton = () => {
    // alert(age);
    SelectedTopContent();
  }

  const rangeFormat =
  endDate === null
    ? moment(startDate).format("MM/DD/YYYY")
    : moment(startDate).format("MM/DD/YYYY") +
      " " +
      "-" +
      " " +
      moment(endDate).format("MM/DD/YYYY");

const rangeDate =
  moment(firstDate).format("MM/DD/YYYY") +
  " " +
  "-" +
  " " +
  moment(lastDate).format("MM/DD/YYYY");


  const onLoadTopContentData = () => {
    // alert(dateRange[1])
    // if (dateRange[0] !== null) {
    console.log(startDate, endDate);

    const startData = moment(startDate).format("MM/DD/YYYY");
    // const endData = moment(endDate).format("MM/DD/YYYY");

    // const end = endDate === null ? formatLastDate : formatYesterdayDate ;
    // alert(end);
    // const type = "meme";
    // const moduleType = "share";
    // const sortType = "desc";
    const range = 10;
    // if(dateRange[1] !== null) {
    //   endData = startData ;
    // }else {
    //   endData = endData;
    // }

    let formData = new FormData();
    formData.append("from_date", formatLastDate);
    formData.append("to_date", formatYesterdayDate);
    formData.append("meme_type", memeType);
    formData.append("module", moduleType);
    formData.append("sort", age);
    formData.append("range", range);
    var myHeaders = new Headers();
    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formData,
    };
    const fetchData = async () => {
      // setLoading(true);
      const response = await fetch(
        "https://ic-dev.lightersideofrealestate.com/ic-scripts_sync_codes/weekly_reports/article_position_list.php",
        requestOptions
      );
      const data = await response.json();
      setOnLoadTopContent({ data });

      setLoading(false);
      // setFdate(formatLastDate);
      // setLdate(formatYesterdayDate);
      console.log(dateRange[0]);
      console.log( memeType);
      // console.log(end);
      if (data) {
        console.log(data);
      }
    };

    fetchData();
    // }
  };


  //when clicking Submit button
  const SelectedTopContent = () => {
    // alert(dateRange[1])
    // if (dateRange[0] !== null) {
    console.log(startDate, endDate);
    const apiTime = "23:59:59";

    const startData = moment(startDate).format("YYYY-MM-DD");
    const endData = moment(endDate).format("YYYY-MM-DD");

    const end = endDate === null ? startData : endData;
    // alert(end);
    const range = 10;
    // if(dateRange[1] !== null) {
    //   endData = startData ;
    // }else {
    //   endData = endData;
    // }

    let formData = new FormData();
    formData.append("from_date", formatLastDate);
    formData.append("to_date", formatYesterdayDate);
    formData.append("meme_type", memeType);
    formData.append("module", moduleType);
    formData.append("sort", age);
    formData.append("range", range);
    var myHeaders = new Headers();
    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formData,
    };
    const fetchData = async () => {
      //  console.log(formData)
      setLoading(true);
      const response = await fetch(
        "https://ic-dev.lightersideofrealestate.com/ic-scripts_sync_codes/weekly_reports/article_position_list.php",
        requestOptions
      );
      const data = await response.json();
      setOnLoadTopContent({ data });
      setLoading(false);
      console.log(dateRange[0]);
      console.log(startData);
      console.log(end);
      // console.log(userData && userData.data.dau_monthly_count);
      if (data) {
        console.log(data);
      }
    };

    fetchData();
    // }
  };


  const configsButton = (
    <MDBox
      display="flex"
      justifyContent="center"
      alignItems="center"
      left="2.15rem"
      height="3.25rem"
      position="absolute"
      zIndex={0}
      color="dark"
      sx={{ cursor: "pointer" }}
      // onClick={handleConfiguratorOpen}
    >
      <Icon fontSize="small" color="inherit">
        event
      </Icon>
    </MDBox>
  );

  useEffect(() => {
    console.log(startDate);
    onLoadTopContentData();
    
    return setDateRange([null, null]);
  }, []);


  return (
    <DashboardLayout>
      <MDBox pt={6} pb={3}>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Card>
              <MDBox
                mx={2}
                mt={-3}
                py={3}
                px={2}
                variant="gradient"
                bgColor="primary"
                borderRadius="lg"
                coloredShadow="info"
              >
                <MDTypography variant="h6" color="white">
                  Top/Bottom Engagement Modules
                </MDTypography>
              </MDBox>

              <MDBox display="flex" alignItems="center" width="90%" className="chart-heading">
                <MDBox
                  borderRadius="lg"
                  display="flex"
                  alignItems="center"
                  justifyContent="space-between"
                  p={3}
                  ml={0}
                >
                  <DatePicker
                    dateFormat="MM/dd/yyy"
                    selectsRange={true}
                    startDate={startDate}
                    value={rangeFormat === "Invalid date" ? rangeDate : rangeFormat}
                    endDate={endDate}
                    // readOnly={true}
                    onChange={(update) => {
                      console.log(update);
                      setDateRange(update);
                    }}
                    onKeyDown={(e) => {
                      e.preventDefault();
                    }}
                    isClearable={false}
                    className="selecting-date"
                  >
                    <div className="datepicker-button ">
                      <button
                        className="choosedate-button"
                        // onClick={() => setDateRange([null, null])}
                      >
                        clear
                      </button>
                    </div>
                  </DatePicker>
                  {configsButton}
                </MDBox>

                <Box sx={{ minWidth: 120, marginTop: 5, marginBottom: 5 }}>
                  <FormControl sx={{ m: 1, minWidth: 130 }}>
                    <InputLabel id="demo-simple-select-helper-label">Module</InputLabel>
                    <Select
                      labelId="demo-simple-select-helper-label"
                      id="demo-simple-select-helper"
                      value={moduleType}
                      label="Age"
                      onChange={moduleChange}
                      className="selecting-values"
                      // sx={{padding:"20px"}}
                    >
                      <MenuItem value="share">Share</MenuItem>
                      <MenuItem value="schedule">Schedule</MenuItem>
                      <MenuItem value="embed">Embed</MenuItem>
                      <MenuItem value="download">Download</MenuItem>
                    </Select>
                    {/* <FormHelperText>With label + helper text</FormHelperText> */}
                  </FormControl>

                  <FormControl sx={{ m: 1, minWidth: 150 }}>
                    <InputLabel id="demo-simple-select-helper-label">Meme Type</InputLabel>
                    <Select
                      labelId="demo-simple-select-helper-label"
                      id="demo-simple-select-helper"
                      value={memeType}
                      label="Age"
                      onChange={memeChange}
                      className="selecting-values"
                      // sx={{padding:"20px"}}
                    >
                      <MenuItem value="article">Article</MenuItem>
                      <MenuItem value="meme">Meme</MenuItem>
                      <MenuItem value="videomeme">Video Meme</MenuItem>
                    </Select>
                    {/* <FormHelperText>With label + helper text</FormHelperText> */}
                  </FormControl>

                  <FormControl sx={{ m: 1, minWidth: 120 }}>
                    <InputLabel id="demo-simple-select-helper-label">Sort</InputLabel>
                    <Select
                      labelId="demo-simple-select-helper-label"
                      id="demo-simple-select-helper"
                      value={age}
                      label="Age"
                      onChange={handleChange}
                      className="selecting-values"
                      // sx={{padding:"20px"}}
                    >
                      <MenuItem value="asc">Ascending</MenuItem>
                      <MenuItem value="desc">Descending</MenuItem>
                    </Select>
                    {/* <FormHelperText>With label + helper text</FormHelperText> */}
                  </FormControl>
                </Box>
                {!isLoading && (
                  <MDButton
                    variant="gradient"
                    color="info"
                    onClick={onClickButton}
                    className="submit"
                  >
                    &nbsp;Submit
                  </MDButton>
                )}
                {isLoading && (
                  <MDButton 
                  variant="gradient" 
                  color="info" 
                  onClick={onClickButton}
                  disabled>
                    <Icon fontSize="small" color="inherit">
                      loop
                    </Icon>
                    &nbsp;Loading...
                  </MDButton>
                )}
              </MDBox>

              <MDBox>
                <DataTable
                  table={{ columns: tColumns, rows: tRows }}
                  isSorted={false}
                  entriesPerPage={false}
                  showTotalEntries={false}
                  noEndBorder
                  topHeader={false}
                  headingOne="Engagement levels on each content"
                  headingTwo="Unique users Engagement levels on each content "
                />
              </MDBox>

            </Card>
          </Grid>
        </Grid>
      </MDBox>
    </DashboardLayout>
  );
}

export default Billing;
