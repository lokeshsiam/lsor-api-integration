/**
=========================================================
* Material Dashboard 2 React - v2.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/
import React, { useState, useEffect } from "react";
// @mui material components
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import Box from "@mui/material/Box";
import Icon from "@mui/material/Icon";

//Date-Picker
import moment from "moment";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDButton from "components/MDButton";

// Material Dashboard 2 React examples
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DataTable from "examples/Tables/DataTable";
import highlightNoContent from "layouts/tables/data/highlightNoContent";
import "../../layouts/dashboard/projects.css";

function Billing() {
  const [age, setAge] = React.useState("desc");
  const [memeType, setMemeType] = useState("meme");
  const [moduleType, setModuleType] = useState("share");
  const [dateRange, setDateRange] = useState([null, null]);
  const [startDate, endDate] = dateRange;

  const [onLoadHighlightNoContent, setOnLoadHighlightNoContent] = useState();

  const [isLoading, setLoading] = useState(false);

  const [dataValues, setDataValues] = useState({
    shared_memes_count: "",
    download_memes_count: "",
    scheduled_memes_count: "",
    shared_video_memes_count: "",
    download_video_memes_count: "",
    scheduled_video_memes_count: "",
    scheduled_posts_count: "",
    embed_posts_count: "",
    shared_posts_count: "",
  });

  const { columns: tColumns, rows: tRows } = highlightNoContent(dataValues);

  const currentDate = new Date();
  //yesterday date - end date
  const yesterdayDate = moment(currentDate).subtract(1, "days");
  const lastDate = yesterdayDate._d;
  const formatYesterdayDate = moment(lastDate).format("YYYY-MM-DD");
  //last seven day date - start date
  const lastSevenDaysDate = moment(currentDate).subtract(1, "week");
  const firstDate = lastSevenDaysDate._d;
  const formatLastDate = moment(firstDate).format("YYYY-MM-DD");

  const handleChange = (event) => {
    setAge(event.target.value);
  };

  const memeChange = (event) => {
    setMemeType(event.target.value);
  };

  const moduleChange = (event) => {
    setModuleType(event.target.value);
  };

  const onClickButton = () => {
    alert(age);
  };

  const onLoadHighlightNoContentData = (valueOne, valueTwo, name) => {
    // alert(dateRange[1])
    // if (dateRange[0] !== null) {

    const memeType = "videomeme";
    const moduleType = "scheduled";

    let formData = new FormData();
    // formData.append("from_date", formatLastDate);
    // formData.append("to_date", formatYesterdayDate);
    formData.append("type", valueOne);
    formData.append("module", valueTwo);
    // formData.append("sort", age);
    // formData.append("range", range);
    var myHeaders = new Headers();
    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formData,
    };
    const fetchData = async (name) => {
      // setLoading(true);
      const response = await fetch(
        "https://ic-dev.lightersideofrealestate.com/ic-scripts_sync_codes/weekly_reports/api_highlight_no_content.php",
        requestOptions
      );
      const data = await response.json();
      //   console.log(name,data.no_content[name])
      //   setOnLoadHighlightNoContent({ data });
      if (data.status === 1) {
        setDataValues((pre) => {
          return { ...pre, [name]: data.no_content[name] };
        });
      }

      setLoading(false);
      // setFdate(formatLastDate);
      // setLdate(formatYesterdayDate);
      console.log(dateRange[0]);
      console.log(memeType);
      // console.log(end);
      if (data) {
        console.log(data);
      }
    };

    fetchData(name);
    // }
  };

  useEffect(() => {
    // console.log(startDate);
    onLoadHighlightNoContentData("videomeme", "shared", "shared_video_memes_count");
    onLoadHighlightNoContentData("videomeme", "scheduled", "scheduled_video_memes_count");
    onLoadHighlightNoContentData("videomeme", "downloaded", "download_video_memes_count");
    onLoadHighlightNoContentData("meme", "shared", "shared_memes_count");
    onLoadHighlightNoContentData("meme", "scheduled", "scheduled_memes_count");
    onLoadHighlightNoContentData("meme", "downloaded", "download_memes_count");
    onLoadHighlightNoContentData("article", "shared", "shared_posts_count");
    onLoadHighlightNoContentData("article", "scheduled", "scheduled_posts_count");
    onLoadHighlightNoContentData("article", "embed", "embed_posts_count");

    // return setDateRange([null, null]);
  }, []);

  return (
    <DashboardLayout>
      <MDBox pt={6} pb={3}>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Card>
              <MDBox
                mx={2}
                mt={-3}
                py={3}
                px={2}
                variant="gradient"
                bgColor="primary"
                borderRadius="lg"
                coloredShadow="info"
              >
                <MDTypography variant="h6" color="white">
                  Life Time - No Content Used Count
                </MDTypography>
              </MDBox>

              <MDBox display="flex" alignItems="center" width="90%" className="chart-heading">
                <MDBox
                  borderRadius="lg"
                  display="flex"
                  alignItems="center"
                  justifyContent="space-between"
                  p={3}
                  ml={0}
                ></MDBox>
              </MDBox>

              <MDBox>
                <DataTable
                  table={{ columns: tColumns, rows: tRows }}
                  isSorted={false}
                  entriesPerPage={false}
                  showTotalEntries={false}
                  noEndBorder
                  topHeader={false}
                  headingOne="Engagement levels on each content"
                  headingTwo="Unique users Engagement levels on each content "
                />
              </MDBox>
            </Card>
          </Grid>
        </Grid>
      </MDBox>
    </DashboardLayout>
  );
}

export default Billing;
