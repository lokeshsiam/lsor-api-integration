/**
=========================================================
* Material Dashboard 2 React - v2.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

import { useState, useEffect } from "react";

// @mui material components
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import Icon from "@mui/material/Icon";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDAlert from "components/MDAlert";
import MDButton from "components/MDButton";
import MDSnackbar from "components/MDSnackbar";

//Date-Picker
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import moment from "moment";

// Material Dashboard 2 React example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from "examples/Footer";

function Notifications() {
  const [successSB, setSuccessSB] = useState(false);
  const [infoSB, setInfoSB] = useState(false);
  const [warningSB, setWarningSB] = useState(false);
  const [errorSB, setErrorSB] = useState(false);

  const [dateRange, setDateRange] = useState([null, null]);
  const [startDate, endDate] = dateRange;
  const [isLoading, setLoading] = useState(false);

  const [fbCountValues,setFbCountValues] = useState();

  const currentDate = new Date();
  //yesterday date - end date
  const yesterdayDate = moment(currentDate).subtract(1, "days");
  const lastDate = yesterdayDate._d;
  const formatYesterdayDate = moment(lastDate).format("YYYY-MM-DD");
  //last seven day date - start date
  const lastSevenDaysDate = moment(currentDate).subtract(1, "week");
  const firstDate = lastSevenDaysDate._d;
  const formatLastDate = moment(firstDate).format("YYYY-MM-DD");

  const onClickButton = () => {
    SelectedFbContent()
  }

  const rangeFormat =
    endDate === null
      ? moment(startDate).format("MM/DD/YYYY")
      : moment(startDate).format("MM/DD/YYYY") +
        " " +
        "-" +
        " " +
        moment(endDate).format("MM/DD/YYYY");

  const rangeDate =
    moment(firstDate).format("MM/DD/YYYY") +
    " " +
    "-" +
    " " +
    moment(lastDate).format("MM/DD/YYYY");

  const configsButton = (
    <MDBox
      display="flex"
      justifyContent="center"
      alignItems="center"
      left="2.15rem"
      height="3.25rem"
      position="absolute"
      zIndex={0}
      color="dark"
      sx={{ cursor: "pointer" }}
      // onClick={handleConfiguratorOpen}
    >
      <Icon fontSize="small" color="inherit">
        event
      </Icon>
    </MDBox>
  );

  const onLoadFbContentData = () => {
    // alert(dateRange[1])
    // if (dateRange[0] !== null) {
    console.log(startDate, endDate);

    const startData = moment(startDate).format("MM/DD/YYYY");
    // const endData = moment(endDate).format("MM/DD/YYYY");

    // const end = endDate === null ? formatLastDate : formatYesterdayDate ;
    // alert(end);
    // const type = "meme";
    // const moduleType = "share";
    // const sortType = "desc";
    const range = 10;
    // if(dateRange[1] !== null) {
    //   endData = startData ;
    // }else {
    //   endData = endData;
    // }

    let formData = new FormData();
    // formData.append("from_date", formatLastDate);
    // formData.append("to_date", formatYesterdayDate);
    var myHeaders = new Headers();
    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formData,
    };
    const fetchData = async () => {
      // setLoading(true);
      const response = await fetch(
        "https://ic-dev.lightersideofrealestate.com/ic-scripts_sync_codes/weekly_reports/api_fbgroup.php",
        requestOptions
      );
      const data = await response.json();
      setFbCountValues({ data });

      setLoading(false);
      // setFdate(formatLastDate);
      // setLdate(formatYesterdayDate);
      console.log(dateRange[0]);
      console.log( formatLastDate);
      // console.log(end);
      if (data) {
        console.log(data);
      }
    };

    fetchData();
    // }
  };

  //when clicking Submit button
  const SelectedFbContent = () => {
    // alert(dateRange[1])
    // if (dateRange[0] !== null) {
    console.log(startDate, endDate);
    const apiTime = "23:59:59";

    const startData = moment(startDate).format("YYYY-MM-DD");
    const endData = moment(endDate).format("YYYY-MM-DD");

    const end = endDate === null ? startData : endData;
    // alert(end);
    const range = 10;
    // if(dateRange[1] !== null) {
    //   endData = startData ;
    // }else {
    //   endData = endData;
    // }

    let formData = new FormData();
    // formData.append("from_date", formatLastDate);
    // formData.append("to_date", formatYesterdayDate);
    var myHeaders = new Headers();
    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formData,
    };
    const fetchData = async () => {
      //  console.log(formData)
      setLoading(true);
      const response = await fetch(
        "https://ic-dev.lightersideofrealestate.com/ic-scripts_sync_codes/weekly_reports/api_fbgroup.php",
        requestOptions
      );
      const data = await response.json();
      setFbCountValues({ data });
      setLoading(false);
      console.log(dateRange[0]);
      console.log(startData);
      console.log(end);
      // console.log(userData && userData.data.dau_monthly_count);
      if (data) {
        console.log(data);
      }
    };

    fetchData();
    // }
  };

  useEffect(() => {
    console.log(startDate);
    onLoadFbContentData();
    
    return setDateRange([null, null]);
  }, []);


  const openSuccessSB = () => setSuccessSB(true);
  const closeSuccessSB = () => setSuccessSB(false);
  const openInfoSB = () => setInfoSB(true);
  const closeInfoSB = () => setInfoSB(false);
  const openWarningSB = () => setWarningSB(true);
  const closeWarningSB = () => setWarningSB(false);
  const openErrorSB = () => setErrorSB(true);
  const closeErrorSB = () => setErrorSB(false);

  const alertContent = (name) => (
    <MDTypography variant="body2" color="white">
      A simple {name} alert with{" "}
      <MDTypography component="a" href="#" variant="body2" fontWeight="medium" color="white">
        an example link
      </MDTypography>
      . Give it a click if you like.
    </MDTypography>
  );

  const renderSuccessSB = (
    <MDSnackbar
      color="success"
      icon="check"
      title="Material Dashboard"
      content="Hello, world! This is a notification message"
      dateTime="11 mins ago"
      open={successSB}
      onClose={closeSuccessSB}
      close={closeSuccessSB}
      bgWhite
    />
  );

  const renderInfoSB = (
    <MDSnackbar
      icon="notifications"
      title="Material Dashboard"
      content="Hello, world! This is a notification message"
      dateTime="11 mins ago"
      open={infoSB}
      onClose={closeInfoSB}
      close={closeInfoSB}
    />
  );

  const renderWarningSB = (
    <MDSnackbar
      color="warning"
      icon="star"
      title="Material Dashboard"
      content="Hello, world! This is a notification message"
      dateTime="11 mins ago"
      open={warningSB}
      onClose={closeWarningSB}
      close={closeWarningSB}
      bgWhite
    />
  );

  const renderErrorSB = (
    <MDSnackbar
      color="error"
      icon="warning"
      title="Material Dashboard"
      content="Hello, world! This is a notification message"
      dateTime="11 mins ago"
      open={errorSB}
      onClose={closeErrorSB}
      close={closeErrorSB}
      bgWhite
    />
  );

  return (
    <DashboardLayout>
      {/* <DashboardNavbar /> */}
      <MDBox mt={6} mb={3}>
        <Grid container spacing={3} justifyContent="center">
          <Grid item xs={12} lg={12}>
            <Card>
              <MDBox p={2}>
                <MDTypography variant="h5">Fb group joined status</MDTypography>
              </MDBox>
              {/* <MDBox display="flex" alignItems="center" width="90%" className="chart-heading">
                <MDBox
                  borderRadius="lg"
                  display="flex"
                  alignItems="center"
                  justifyContent="space-between"
                  p={3}
                  ml={0}
                >
                  <DatePicker
                    dateFormat="MM/dd/yyy"
                    selectsRange={true}
                    startDate={startDate}
                    value={rangeFormat === "Invalid date" ? rangeDate : rangeFormat}
                    endDate={endDate}
                    // readOnly={true}
                    onChange={(update) => {
                      console.log(update);
                      setDateRange(update);
                    }}
                    onKeyDown={(e) => {
                      e.preventDefault();
                    }}
                    isClearable={false}
                    className="selecting-date"
                  >
                    <div className="datepicker-button ">
                      <button
                        className="choosedate-button"
                        // onClick={() => setDateRange([null, null])}
                      >
                        clear
                      </button>
                    </div>
                  </DatePicker>
                  {configsButton}
                </MDBox>
                {!isLoading && (
                  <MDButton
                    variant="gradient"
                    color="info"
                    onClick={onClickButton}
                    className="submit"
                  >
                    &nbsp;Submit
                  </MDButton>
                )}
                {isLoading && (
                  <MDButton
                    variant="gradient"
                    color="info"
                    onClick={onClickButton}
                    disabled
                  >
                    <Icon fontSize="small" color="inherit">
                      loop
                    </Icon>
                    &nbsp;Loading...
                  </MDButton>
                )}
              </MDBox> */}


              <MDBox pl={3} textAlign="end">
              <Grid container direction="row" justifyContent="start">
                <Grid item xs={12} md={5} lg={5}>
                  <MDTypography
                    component="span"
                    variant="button"
                    fontSize="medium"
                    fontWeight="bold"
                  >
                    Fb Joined Members&nbsp;&nbsp;=
                  </MDTypography>
                </Grid>
                <Grid item xs={12} md={3} lg={3}>
                  <MDTypography
                    component="span"
                    variant="button"
                    fontSize="medium"
                    fontWeight="bold"
                    color="success"
                  >
                    &nbsp;{fbCountValues?.data?.fb_joined_members}
                  </MDTypography>
                </Grid>

                <Grid item xs={12} md={5} lg={5}>
                  <MDTypography
                    component="span"
                    variant="button"
                    fontSize="medium"
                    fontWeight="bold"
                  >
                     Fb left Members&nbsp;&nbsp;=
                  </MDTypography>
                </Grid>
                <Grid item xs={12} md={3} lg={3}>
                  <MDTypography
                    component="span"
                    variant="button"
                    fontSize="medium"
                    fontWeight="bold"
                    color="success"
                  >
                    &nbsp;{fbCountValues?.data?.fb_left_members}
                  </MDTypography>
                </Grid>

                <Grid item xs={12} md={5} lg={5}>
                  <MDTypography
                    component="span"
                    variant="button"
                    fontSize="medium"
                    fontWeight="bold"
                  >
                    Fb Empty Members&nbsp;&nbsp;=
                  </MDTypography>
                </Grid>
                <Grid item xs={12} md={3} lg={3}>
                  <MDTypography
                    component="span"
                    variant="button"
                    fontSize="medium"
                    fontWeight="bold"
                    color="success"
                  >
                   &nbsp;{fbCountValues?.data?.fb_empty_members}
                  </MDTypography>
                </Grid>
              </Grid>
              </MDBox>
              
            </Card>
          </Grid>
        </Grid>
      </MDBox>
      {/* <Footer /> */}
    </DashboardLayout>
  );
}

export default Notifications;
