import React from 'react';
import MDBox from "components/MDBox";
import CircularProgress from "@mui/material/CircularProgress";

const Loader = () => {
  return (
    <MDBox sx={{ display: "flex", justifyContent: "center"}} pt={1}>
        <CircularProgress size="1.5rem" color="inherit" />
      </MDBox>
  )
}

export default Loader;
