import React from "react";
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";

function Header({header})  {
  return (
    <MDBox
      mx={2}
      mt={-3}
      py={3}
      px={2}
      variant="gradient"
      bgColor="primary"
      borderRadius="lg"
      coloredShadow="info"
    >
      <MDTypography variant="h6" color="white">
        {header}
      </MDTypography>
    </MDBox>
  );
};

export default Header;
